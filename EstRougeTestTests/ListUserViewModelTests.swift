//
//  ListUserViewModelTests.swift
//  EstRougeTestTests
//
//  Created by Trương Văn Kiên on 9/17/19.
//  Copyright © 2019 Trương Văn Kiên. All rights reserved.
//

import XCTest
import RxBlocking

@testable import EstRougeTest

class ListUserViewModelTests: XCTestCase {
    
    var viewModel: ListUserViewModel!

    override func setUp() {
        viewModel = ListUserViewModel()
    }

    override func tearDown() {
        viewModel = nil
        super.tearDown()
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testRequestListUser() {
        viewModel.requestListUser()
        XCTAssertEqual(try viewModel.listUser.skip(1).toBlocking().first()?.count, 30)
    }
}
