//
//  ProfileViewModelTests.swift
//  EstRougeTestTests
//
//  Created by Trương Văn Kiên on 9/17/19.
//  Copyright © 2019 Trương Văn Kiên. All rights reserved.
//

import XCTest
@testable import EstRougeTest

class ProfileViewModelTests: XCTestCase {
    
    var viewModel: ProfileViewModel!

    override func setUp() {
        let userID = 1
        viewModel = ProfileViewModel(userID: userID)
    }

    override func tearDown() {
        viewModel = nil
        super.tearDown()
    }
    
    func testFollower() {
        viewModel.requestUser()
        XCTAssertEqual(try viewModel.user.skip(1).toBlocking().first()??.followers, 19)
    }
    
    func testFollowing() {
        viewModel.requestUser()
        XCTAssertEqual(try viewModel.user.skip(1).toBlocking().first()??.following, 0)
    }
}
