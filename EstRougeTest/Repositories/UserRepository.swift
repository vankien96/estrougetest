//
//  UserRepository.swift
//  EstRougeTest
//
//  Created by Trương Văn Kiên on 9/16/19.
//  Copyright © 2019 Trương Văn Kiên. All rights reserved.
//

import Foundation
import RxSwift
import ObjectMapper

protocol UserRepository {
    func requestListUser() -> Observable<[User]>
    func requestProfile(userID: Int) -> Observable<User?>
}

class UserRepositoryImpl: UserRepository {
    
    static let shared = UserRepositoryImpl()
    
    private init() { }
    
    func requestListUser() -> Observable<[User]> {
        let listUserRequest = ListUserRequest()
        return APIService.shared.request(input: listUserRequest).map({ Mapper<User>().mapArray(JSONObject: $0) ?? [] })
    }
    
    func requestProfile(userID: Int) -> Observable<User?> {
        let profileRequest = ProfileRequest(userID: userID)
        return APIService.shared.request(input: profileRequest).map({ User(JSON: ($0 as? [String: Any]) ?? [:] )})
    }
}
