//
//  LocalizableViewController.swift
//  EstRougeTest
//
//  Created by Trương Văn Kiên on 9/16/19.
//  Copyright © 2019 Trương Văn Kiên. All rights reserved.
//

import UIKit

open class LocalizableViewController: UIViewController {
    
    open func onUpdateLocalize() {
        self.view.onUpdateLocalize()
    }
}
