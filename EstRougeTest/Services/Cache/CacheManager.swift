//
//  CacheManager.swift
//  EstRougeTest
//
//  Created by Trương Văn Kiên on 9/17/19.
//  Copyright © 2019 Trương Văn Kiên. All rights reserved.
//

import Foundation
import CoreData

class CacheManager {
    
    static var shared = CacheManager()
    
    func read(urlString: String) -> Any? {
        let context = CoreDataManager.persistentContainer.viewContext
        let fetchRequest: NSFetchRequest<CacheModel> = CacheModel.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "url = %@", urlString)
        do {
            if let loadedData = try context.fetch(fetchRequest).first,
                let cachedData = loadedData.data,
                let data = NSKeyedUnarchiver.unarchiveObject(with: cachedData) {
                return data
            }
        } catch {
            return nil
        }
        return nil
    }
    
    func write(urlString: String, data: Any) throws -> Void {
        let data = NSKeyedArchiver.archivedData(withRootObject: data)
        
        if let cacheModel: CacheModel = CoreDataManager.getEntity() {
            cacheModel.url = urlString
            cacheModel.data = data
            CoreDataManager.saveContext()
        }
    }
}
