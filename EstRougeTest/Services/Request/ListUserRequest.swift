//
//  UserRequest.swift
//  EstRougeTest
//
//  Created by Trương Văn Kiên on 9/16/19.
//  Copyright © 2019 Trương Văn Kiên. All rights reserved.
//

import Foundation

class ListUserRequest: BaseRequest {
    
    init() {
        super.init(url: URLs.shared.APIGetListUser, requestType: .get)
    }
}
