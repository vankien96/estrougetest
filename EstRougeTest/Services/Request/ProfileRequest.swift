//
//  ProfileRequest.swift
//  EstRougeTest
//
//  Created by Trương Văn Kiên on 9/16/19.
//  Copyright © 2019 Trương Văn Kiên. All rights reserved.
//

import Foundation

class ProfileRequest: BaseRequest {
    
    init(userID: Int) {
        let url = String(format: URLs.shared.APIGetProfile, "\(userID)")
        super.init(url: url, requestType: .get)
    }
}
