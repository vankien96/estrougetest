//
//  APIUrls.swift
//  EstRougeTest
//
//  Created by Trương Văn Kiên on 9/16/19.
//  Copyright © 2019 Trương Văn Kiên. All rights reserved.
//

import Foundation

struct URLs {
    
    static var shared: URLs = URLs()
    
    private init() { }
    
//    var APILoginUrl: String { return APIBaseUrl + "/login" }
//
//    var APIRefreshToken: String { return APIBaseUrl + "/refresh_token" }
    
    var APIGetListUser: String { return APIBaseUrl + "/users"}
    
    var APIGetProfile: String { return APIBaseUrl + "/users/%@" }
    
    private var APIBaseUrl: String {
        get {
            return "https://api.github.com"
        }
    }
}
