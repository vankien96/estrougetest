//
//  APIService.swift
//  EstRougeTest
//
//  Created by Trương Văn Kiên on 9/16/19.
//  Copyright © 2019 Trương Văn Kiên. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import RxSwift

class AuthSessionManager: SessionManager {

    public func authRequest(input: BaseRequest, completion: @escaping ((DataRequest) -> Void)) {
        guard let token = TokenManager.shared.getToken() else { return }
        let dataRequest = request(input.url, method: input.requestType, parameters: input.body, encoding: input.encoding)
        if token.isValidAccessToken() {
            completion(dataRequest)
        } else if token.isValidRefreshToken() {
            //Refresh token here
        } else {
            // logout
        }
    }
}

class APIService {
    
    static let shared = APIService()
    private var alamofireManager = AuthSessionManager()
    
    init() {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 30
        configuration.timeoutIntervalForResource = 30
        alamofireManager.adapter = APIRequestAdapter()
    }
    
    func requestWithAuth(input: BaseRequest) ->  Observable<Any> {
        return Observable.create { observer in
            self.alamofireManager.authRequest(input: input, completion: { (authRequest) in
                authRequest.validate(statusCode: 200..<500).responseJSON { response in
                    switch response.result {
                    case .success(let value):
                        if let statusCode = response.response?.statusCode {
                            if statusCode == 200 {
                                observer.onNext(value)
                                observer.onCompleted()
                            } else {
                                if let object = Mapper<ErrorResponse>().map(JSONObject: value) {
                                    object.code = statusCode
                                    observer.onError(BaseError.apiFailure(error: object))
                                } else {
                                    observer.onError(BaseError.httpError(httpCode: statusCode))
                                }
                            }
                        } else {
                            observer.onError(BaseError.unexpectedError)
                        }
                    case .failure:
                        let error = NetworkReachabilityManager()!.isReachable ? BaseError.unexpectedError : BaseError.networkError
                        observer.onError(error)
                    }
                }
            })
            return Disposables.create()
        }
    }
    
    func request(input: BaseRequest) ->  Observable<Any> {
        return Observable.create { observer in
            DispatchQueue.main.async {
                if let cached = CacheManager.shared.read(urlString: input.url) {
                    observer.onNext(cached)
                }   
            }
            self.alamofireManager.request(input.url, method: input.requestType, parameters: input.body, encoding: input.encoding)
                .validate(statusCode: 200..<500)
                .responseJSON { response in
                    switch response.result {
                    case .success(let value):
                        if let statusCode = response.response?.statusCode {
                            if statusCode == 200 {
                                observer.onNext(value)
                                try? CacheManager.shared.write(urlString: input.url, data: value)
                                observer.onCompleted()
                            } else {
                                if let object = Mapper<ErrorResponse>().map(JSONObject: value) {
                                    object.code = statusCode
                                    observer.onError(BaseError.apiFailure(error: object))
                                } else {
                                    observer.onError(BaseError.httpError(httpCode: statusCode))
                                }
                            }
                        } else {
                            observer.onError(BaseError.unexpectedError)
                        }
                    case .failure:
                        let error = NetworkReachabilityManager()!.isReachable ? BaseError.unexpectedError : BaseError.networkError
                        observer.onError(error)
                    }
            }
            return Disposables.create()
        }
    }
}
