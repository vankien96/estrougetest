//
//  CacheModel.swift
//  EstRougeTest
//
//  Created by Trương Văn Kiên on 9/17/19.
//  Copyright © 2019 Trương Văn Kiên. All rights reserved.
//

import Foundation
import CoreData

@objc(CacheModel)
class CacheModel: NSManagedObject {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<CacheModel> {
        return NSFetchRequest<CacheModel>(entityName: "CacheModel")
    }
    
    @NSManaged public var url: String?
    @NSManaged public var data: Data?
}
