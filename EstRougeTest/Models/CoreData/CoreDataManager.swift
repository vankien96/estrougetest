//
//  CoreDataManager.swift
//  EstRougeTest
//
//  Created by Trương Văn Kiên on 9/17/19.
//  Copyright © 2019 Trương Văn Kiên. All rights reserved.
//

import CoreData

class CoreDataManager {
    
    static var persistentContainer: PersistentContainer = PersistentContainerImpl.shared
    
    class func getEntity<T: NSManagedObject>() -> T? {
        if #available(iOS 10, *) {
            let managedObject = T(context: persistentContainer.viewContext)
            return managedObject
        } else {
            guard let entityDescription = NSEntityDescription.entity(forEntityName: NSStringFromClass(T.self), in: persistentContainer.viewContext) else { return nil }
            let managedObject = T(entity: entityDescription, insertInto: persistentContainer.viewContext)
            return managedObject
        }
    }
    
    class func getEntity(name: String) -> NSManagedObject? {
        guard let entityDescription = NSEntityDescription.entity(forEntityName: name, in: persistentContainer.viewContext) else { return nil }
        let managedObject = NSManagedObject(entity: entityDescription, insertInto: persistentContainer.viewContext)
        return managedObject
    }
    
    // MARK: - Core Data Saving support
    class func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {}
        }
    }
}

protocol PersistentContainer {
    var viewContext: NSManagedObjectContext { get }
    var managedObjectModel: NSManagedObjectModel { get }
    var persistentStoreCoordinator: NSPersistentStoreCoordinator? { get }
    func defaultDirectoryURL() -> URL?
}

fileprivate class PersistentContainerImpl: PersistentContainer {
    
    static let shared = PersistentContainerImpl()
    
    lazy var viewContext: NSManagedObjectContext = {
        let coordinator = persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        let modelURL = Bundle(for: PersistentContainerImpl.self).url(forResource: "EstRougeTest", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator? = {
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModel)
        guard let url = defaultDirectoryURL()?.appendingPathComponent("EstRougeTest.sqlite") else {
            return nil
        }
        let options = [NSMigratePersistentStoresAutomaticallyOption: true,
                      NSInferMappingModelAutomaticallyOption: true]
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: options)
        } catch {
            return nil
        }
        return coordinator
    }()
    
    func defaultDirectoryURL() -> URL? {
        let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last
        return url
    }
}

