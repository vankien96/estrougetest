//
//  TokenManager.swift
//  EstRougeTest
//
//  Created by Trương Văn Kiên on 9/16/19.
//  Copyright © 2019 Trương Văn Kiên. All rights reserved.
//

import Foundation
import RxCocoa
import UICKeyChainStore
import Alamofire
import ObjectMapper

private struct KeyChainKey {
    static let userToken = "userToken"
}

class TokenManager {
    
    static let shared = TokenManager()
    let keychain = UICKeyChainStore()
    private var token: Token?
    
    var isChangeToken = BehaviorRelay<Bool>(value: false)
    
    private init() { }
    
    func saveToken(token: Token) {
        self.token = token
        let tokenData = NSKeyedArchiver.archivedData(withRootObject: token)
        keychain.setData(tokenData, forKey: KeyChainKey.userToken)
        isChangeToken.accept(true)
    }
    
    func getToken() -> Token? {
        if let token = token {
            return token
        }
        guard let tokenData = keychain.data(forKey: KeyChainKey.userToken),
            let token = NSKeyedUnarchiver.unarchiveObject(with: tokenData) as? Token else { return nil }
        self.token = token
        return token
    }
    
    func removeToken() {
        keychain[KeyChainKey.userToken] = nil
        self.token = nil
    }
}
