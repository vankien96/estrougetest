//
//  User.swift
//  EstRougeTest
//
//  Created by Trương Văn Kiên on 9/16/19.
//  Copyright © 2019 Trương Văn Kiên. All rights reserved.
//

import Foundation
import ObjectMapper

class User: Mappable {
    
    var id: Int!
    var login = ""
    var name = ""
    var avatar: String?
    var repoUrl = ""
    var htmlUrl = ""
    var location: String?
    var bio: String?
    var publicRepo: Int = 0
    var followers: Int = 0
    var following: Int = 0
    
    init() {
    }
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        login <- map["login"]
        name <- map["name"]
        avatar <- map["avatar_url"]
        repoUrl <- map["url"]
        htmlUrl <- map["html_url"]
        publicRepo <- map["public_repos"]
        followers <- map["followers"]
        following <- map["following"]
        location <- map["location"]
        bio <- map["bio"]
    }
    
    func avatarUrl() -> URL? {
        return URL(string: avatar ?? "")
    }
}
