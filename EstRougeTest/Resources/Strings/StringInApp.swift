//
//  StringInApp.swift
//  EstRougeTest
//
//  Created by Trương Văn Kiên on 9/16/19.
//  Copyright © 2019 Trương Văn Kiên. All rights reserved.
//

import Foundation
import Localize_Swift

struct Strings {
    
    struct Main {
        static let appName = "EstRougeTest"
        static var no: String { return "No".localized() }
        static var yes: String { return "Yes".localized() }
        static var ok: String { return "OK".localized() }
        static var done: String { return "Done".localized() }
        static var noInternetConnection: String { return "No internet connection. Please check and try again!".localized() }
        static var pullToRefresh: String { return "Pull To Refresh".localized() }
    }
    
    struct Error {
        static var unexpectedError: String { return "Unexpected Error".localized() }
    }
    
    struct TitleScreen {
        static var listUser: String { return "List User".localized() }
        static var profile: String { return "Profile".localized() }
    }
}
