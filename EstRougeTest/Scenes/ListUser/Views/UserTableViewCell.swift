//
//  UserTableViewCell.swift
//  EstRougeTest
//
//  Created by Trương Văn Kiên on 9/16/19.
//  Copyright © 2019 Trương Văn Kiên. All rights reserved.
//

import UIKit

class UserTableViewCell: UITableViewCell {
    
    enum ViewType {
        case fromListUser, fromProfile
    }
    
    @IBOutlet private weak var avatarImageView: UIImageView!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var htmlUrlLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setup(user: User?, type: ViewType) {
        guard let user = user else { return }
        self.avatarImageView.setAvatar(url: user.avatarUrl())
        switch type {
        case .fromListUser:
            self.nameLabel.text = user.login
            self.htmlUrlLabel.text = user.htmlUrl
        case .fromProfile:
            self.nameLabel.text = user.name
            self.htmlUrlLabel.text = user.location
        }
    }
}
