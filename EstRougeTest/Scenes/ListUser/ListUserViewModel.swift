//
//  ListUserViewModel.swift
//  EstRougeTest
//
//  Created by Trương Văn Kiên on 9/16/19.
//  Copyright © 2019 Trương Văn Kiên. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class ListUserViewModel: BaseViewModel {
    
    private var userRepository: UserRepository
    
    private(set) var listUser: BehaviorRelay<[User]>
    
    override init() {
        self.userRepository = UserRepositoryImpl.shared
        self.listUser = BehaviorRelay<[User]>(value: [])
    }
    
    func requestListUser() {
        self.isShowLoading.accept(true)
        userRepository.requestListUser()
            .subscribe(onNext: { [weak self] (users) in
                self?.isShowLoading.accept(false)
                self?.listUser.accept(users)
            }, onError: { [weak self] (error) in
                self?.isShowLoading.accept(false)
                self?.error.accept(error)
            }).disposed(by: bag)
    }
    
    func numberOfUser() -> Int {
        return listUser.value.count
    }
    
    func userAt(indexPath: IndexPath) -> User? {
        guard indexPath.row < listUser.value.count else { return nil }
        return self.listUser.value[indexPath.row]
    }
}
