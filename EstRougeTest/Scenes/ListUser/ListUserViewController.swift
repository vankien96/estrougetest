//
//  ViewController.swift
//  EstRougeTest
//
//  Created by Trương Văn Kiên on 9/16/19.
//  Copyright © 2019 Trương Văn Kiên. All rights reserved.
//

import UIKit

class ListUserViewController: BaseUIViewController {
    
    @IBOutlet private weak var tableView: UITableView!
    private var refreshControl: UIRefreshControl!
    
    private var viewModel: ListUserViewModel!
    private var navigator: ListUserNavigator!
    private var isRefreshing: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = ListUserViewModel()
        navigator = ListUserNavigatorImpl(viewController: self)
        setupUI()
        bindUI()
        requestData()
    }
    
    private func setupUI() {
        self.title = Strings.TitleScreen.listUser
        setupTableView()
        setUpRefreshControl()
    }
    
    private func setupTableView() {
        tableView.registerCell(ofType: UserTableViewCell.self)
        self.tableView.tableFooterView = UIView()
    }
    
    private func setUpRefreshControl(){
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: Strings.Main.pullToRefresh)
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        tableView.addSubview(refreshControl)
    }
    
    @objc private func refreshData() {
        self.viewModel.requestListUser()
        self.isRefreshing = true
    }
}

extension ListUserViewController {
    
    private func requestData() {
        viewModel.requestListUser()
    }
    
    private func bindUI() {
        viewModel.listUser.asDriver().skip(1)
            .drive(onNext: { [weak self] (_) in
                guard let `self` = self else { return }
                self.tableView.reloadData()
                if self.isRefreshing {
                    self.isRefreshing = false
                    self.refreshControl.endRefreshing()
                }
            }).disposed(by: bag)
        
        viewModel.error.asDriver().skip(1)
            .drive(onNext: { [weak self] (error) in
                guard let error = error else { return }
                self?.showAlertWithMessage(message: error.localizedDescription)
            }).disposed(by: bag)
        
        viewModel.isShowLoading.asDriver().skip(1)
            .drive(onNext: { (isShow) in
                //Handle show loading
            }).disposed(by: bag)
    }
}

extension ListUserViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfUser()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UserTableViewCell = tableView.dequeueCell(indexPath: indexPath)
        cell.setup(user: viewModel.userAt(indexPath: indexPath), type: .fromListUser)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let user = viewModel.userAt(indexPath: indexPath) else { return }
        self.navigator.showProfileScreen(userID: user.id)
    }
}

