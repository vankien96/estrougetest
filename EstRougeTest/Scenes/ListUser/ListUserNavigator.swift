//
//  ListUserNavigato.swift
//  EstRougeTest
//
//  Created by Trương Văn Kiên on 9/17/19.
//  Copyright © 2019 Trương Văn Kiên. All rights reserved.
//

import Foundation

protocol ListUserNavigator {
    func showProfileScreen(userID: Int)
}

class ListUserNavigatorImpl: BaseNavigator, ListUserNavigator {
    
    func showProfileScreen(userID: Int) {
        let viewController = ProfileViewController.created(userID: userID)
        self.viewController?.navigationController?.pushViewController(viewController, animated: true)
    }
}
