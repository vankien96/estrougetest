//
//  BaseViewModel.swift
//  EstRougeTest
//
//  Created by Trương Văn Kiên on 9/16/19.
//  Copyright © 2019 Trương Văn Kiên. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class BaseViewModel {
    
    let bag = DisposeBag()
    let error: BehaviorRelay<Error?>
    let isShowLoading: BehaviorRelay<Bool>
    
    init() {
        error = BehaviorRelay<Error?>(value: nil)
        isShowLoading = BehaviorRelay<Bool>(value: false)
    }
}
