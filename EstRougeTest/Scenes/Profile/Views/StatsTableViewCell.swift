//
//  StatsTableViewCell.swift
//  EstRougeTest
//
//  Created by Trương Văn Kiên on 9/16/19.
//  Copyright © 2019 Trương Văn Kiên. All rights reserved.
//

import UIKit

class StatsTableViewCell: UITableViewCell {
    
    @IBOutlet private weak var publicRepoCountLabel: UILabel!
    @IBOutlet private weak var followersCountLabel: UILabel!
    @IBOutlet private weak var followingCountLabel: UILabel!
    
    var user: User? {
        didSet {
            self.publicRepoCountLabel.text = "\(user?.publicRepo ?? 0)"
            self.followersCountLabel.text = "\(user?.followers ?? 0)"
            self.followingCountLabel.text = "\(user?.following ?? 0)"
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
