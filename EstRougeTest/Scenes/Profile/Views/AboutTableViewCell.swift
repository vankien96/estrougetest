//
//  AboutTableViewCell.swift
//  EstRougeTest
//
//  Created by Trương Văn Kiên on 9/16/19.
//  Copyright © 2019 Trương Văn Kiên. All rights reserved.
//

import UIKit

class AboutTableViewCell: UITableViewCell {

    @IBOutlet private weak var aboutLabel: UILabel!
    
    var about: String! {
        didSet {
            aboutLabel.text = about
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
