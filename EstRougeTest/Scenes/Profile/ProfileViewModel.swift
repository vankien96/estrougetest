//
//  ProfileViewModel.swift
//  EstRougeTest
//
//  Created by Trương Văn Kiên on 9/16/19.
//  Copyright © 2019 Trương Văn Kiên. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class ProfileViewModel: BaseViewModel {
    
    private var userID: Int!
    private(set) var user: BehaviorRelay<User?>
    
    private var userRepository: UserRepository
    
    init(userID: Int) {
        self.userID = userID
        userRepository = UserRepositoryImpl.shared
        user = BehaviorRelay<User?>(value: nil)
    }
    
    func requestUser() {
        self.isShowLoading.accept(true)
        userRepository.requestProfile(userID: self.userID)
            .subscribe(onNext: { [weak self] (user) in
                self?.isShowLoading.accept(false)
                guard let user = user else { return }
                self?.user.accept(user)
                }, onError: { [weak self] (error) in
                    self?.isShowLoading.accept(false)
                    self?.error.accept(error)
            }).disposed(by: bag)
    }
    
    func getUser() -> User? {
        return self.user.value
    }
}
