//
//  ProfileViewController.swift
//  EstRougeTest
//
//  Created by Trương Văn Kiên on 9/16/19.
//  Copyright © 2019 Trương Văn Kiên. All rights reserved.
//

import UIKit

class ProfileViewController: BaseUIViewController {
    
    @IBOutlet private weak var tableView: UITableView!
    private var refreshControl: UIRefreshControl!
    
    private var viewModel: ProfileViewModel!
    private var isRefreshing: Bool = false
    
    private var identifiers: [String] = []
    
    static func created(userID: Int) -> ProfileViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        viewController.viewModel = ProfileViewModel(userID: userID)
        return viewController
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        bindUI()
        requestData()
    }
    
    private func setupUI() {
        self.title = Strings.TitleScreen.profile
        setupTableView()
        setUpRefreshControl()
    }
    
    private func setupTableView() {
        self.tableView.registerCell(ofType: UserTableViewCell.self)
        self.tableView.registerCell(ofType: AboutTableViewCell.self)
        self.tableView.registerCell(ofType: StatsTableViewCell.self)
        self.tableView.tableFooterView = UIView()
        self.identifiers = [UserTableViewCell.identifier, AboutTableViewCell.identifier, StatsTableViewCell.identifier]
    }
    
    private func setUpRefreshControl(){
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: Strings.Main.pullToRefresh)
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        tableView.addSubview(refreshControl)
    }
    
    @objc private func refreshData() {
        self.viewModel.requestUser()
        self.isRefreshing = true
    }
}

extension ProfileViewController {
    
    private func requestData() {
        self.viewModel.requestUser()
    }
    
    private func bindUI() {
        viewModel.user.asDriver().skip(1)
            .drive(onNext: { [weak self] (_) in
                guard let `self` = self else { return }
                self.tableView.reloadData()
                if self.isRefreshing {
                    self.isRefreshing = false
                    self.refreshControl.endRefreshing()
                }
            }).disposed(by: bag)
        
        viewModel.error.asDriver().skip(1)
            .drive(onNext: { [weak self] (error) in
                guard let error = error else { return }
                self?.showAlertWithMessage(message: error.localizedDescription)
            }).disposed(by: bag)
        
        viewModel.isShowLoading.asDriver().skip(1)
            .drive(onNext: { (isShow) in
                //Handle show loading
            }).disposed(by: bag)
    }
}

extension ProfileViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return identifiers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: identifiers[indexPath.row], for: indexPath)
        configCell(cell: cell)
        return cell
    }
    
    private func configCell(cell: UITableViewCell) {
        switch cell {
        case let cell as UserTableViewCell:
            cell.setup(user: viewModel.getUser(), type: .fromProfile)
        case let cell as AboutTableViewCell:
            cell.about = viewModel.getUser()?.bio ?? ""
        case let cell as StatsTableViewCell:
            cell.user = viewModel.getUser()
        default:
            break
        }
    }
}
